<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Rutas personalizadas de vehiculos
Route::group(['prefix' => 'vehicles','middleware' => 'auth:api'], function () {
  Route::get('/getAvailable', 'VehicleController@getAvailable');
});


//Rutas personalizadas de ordenes
Route::group(['prefix' => 'orders','middleware' => 'auth:api'], function () {
  Route::post('finish/{order_id}', 'OrderController@finish');
  Route::post('activate/{order_id}', 'OrderController@activate');
  Route::post('cancel/{order_id}', 'OrderController@cancel');
  Route::get('getInProgress', 'OrderController@getInProgress');
  Route::get('getFinished', 'OrderController@getFinished');
  Route::get('getCanceled', 'OrderController@getCanceled');
  Route::get('getCreated', 'OrderController@getCreated');
});

//Rutas personalizadas para el login y registro
Route::group(['prefix' => 'session'], function () {
  Route::post('register', 'UserController@register');
  Route::post('login', 'UserController@login');
  Route::post('logout/{user}', 'UserController@logout');
});

//Rutas personalizadas para usuarios
Route::group(['prefix' => 'users'], function () {
  Route::get('getOrders/{user}', 'UserController@getOrders');
});

Route::resource('users','UserController')->middleware('auth:api');
Route::resource('vehicles','VehicleController')->middleware('auth:api');
Route::resource('orders','OrderController')->middleware('auth:api');