<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\OauthAccessToken;
use App\Http\Requests\UserStorageRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(User::all(),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStorageRequest $request)
    {
        $user = User::create(
            [
                'name' => $request->name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'birthday' => $request->birthday
            ]
        );
        return response()->json($user,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $user->orders;
        return response()->json($user,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());
        return response()->json($user,200);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(null,204);
    }

    public function register(UserStorageRequest $request) {
        $user = User::create(
            [
                'name' => $request->name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'birthday' => $request->birthday
            ]
        );
        $accessToken = $user->createToken('authToken')->accessToken;
        return response()->json(['user' => $user, 'access_token' => $accessToken],200);

    }

    public function login(LoginRequest $request){
        $validateData = $request->validate(
            [
                'password' => 'required',
                'email' => 'email|required',
            ]
        );
        if(!auth()->attempt(['password' => $request->password, 'email' => $request->email])){
            return response()->json(['message' => 'El usuario y el password no coinciden'], 404);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response()->json(['user' => auth()->user(), 'access_token' => $accessToken],200);
    }

    public function logout($user){
        $tokens = OauthAccessToken::where('user_id',$user)->get();
        foreach ($tokens as $token) {
            OauthAccessToken::destroy($token->id);
        }
        return response()->json('Se ha realizado el logout con exito',200);
    }

    public function getOrders($user){
        return response()->json(Order::where('user_id',$user)->with('vehicle')->orderBy('created_at','desc')->get(),200);
    }
}
