<?php

namespace App\Http\Controllers;

use App\Order;
use App\Vehicle;
use App\Http\Requests\OrderStorageRequest;
use App\Http\Requests\ReturnVehicleRequest;
use App\Http\Requests\ActivateOrderRequest;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        return response()->json(Order::with(['user', 'vehicle'])->orderBy('created_at','desc')->get(),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderStorageRequest $request)
    {
        $vehicle = Vehicle::find($request->vehicle_id);
        if($vehicle && $vehicle->available){
            $order = Order::create(
                [
                    'pickup_date' => $request->pickup_date,
                    'reservation_date' => $request->reservation_date,
                    'theoretical_return_date' => $request->theoretical_return_date,
                    'return_date' => $request->return_date,
                    'status' => 'creada',
                    'user_id' => $request->user_id,
                    'vehicle_id' => $request->vehicle_id
                ]
            );
            Vehicle::where('id',$request->vehicle_id)->update(['available' => false]);
            return response()->json($order,201);
        }else if(!$vehicle){
            return response()->json(['message' => 'El vehiculo no existe dentro dentro de la base de datos.'],400);
        }else{
            return response()->json(['message' => 'El vehiculo no se encuentra disponible para reservar'],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return response()->json($order,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(OrderStorageRequest $request, Order $order)
    {
        $order->update($request->all());
        return response()->json($order,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return response()->json(null,204);
    }

    /**
     * Metodo para Finalizar la reserva de un vehiculo
     * @param Date $return_date
     * @param integer $order_id
     * @param integer $vehicle_id
     */

    public function finish(ReturnVehicleRequest $request,$order_id){
        $order = Order::find($order_id);
        if($order->status !== 'finalizada'){
            $order->update(['return_date' => $request->return_date, 'status' => 'finalizada']);
            Vehicle::where('id', $order->vehicle_id)->update(['available' => true]);
            return response()->json($order,200);
        }else{
            return response()->json(['message' => 'La reservacion ya se encuentra finalizada'],400);
        }
       
    }

    public function activate(ActivateOrderRequest $request,$order_id){
        $order = Order::find($order_id);
        if($order->status === 'activa'){
            return response()->json(['message' => 'La reserva ya se encuentra activa'], 400);
        } else if($order->status === 'finalizada'){
            return response()->json(['message' => 'La reserva ya se encuentra finalizada'], 400);
        } else{
            $order->update(['status' => 'activa', 'pickup_date' => $request->pickup_date]);
            return response()->json($order,200);
        }
    }

    public function cancel($order_id){
        $order = Order::find($order_id);        
        if($order->status === 'activa'){
            return response()->json(['message' => 'La reserva ya se encuentra activa'], 400);
        } else if($order->status === 'finalizada'){
            return response()->json(['message' => 'La reserva ya se encuentra finalizada'], 400);
        } else{
            $order->update(['status' => 'cancelada']);
            Vehicle::where('id', $order->vehicle_id)->update(['available' => true]);
            return response()->json($order,200);
        }
    }

    public function getInProgress(){
        return response()->json(Order::with(['user','vehicle'])->where('return_date', null)->orderBy('created_at','desc')->get(),200);
    }

    public function getFinished(){
        return response()->json(Order::with(['user','vehicle'])->where('return_date', '<>' ,null)->orderBy('created_at','desc')->get(),200);
    }

    public function getCanceled(){
        return response()->json(Order::with(['user','vehicle'])->where('status','cancelada')->orderBy('created_at','desc')->get(),200);
    }

    public function getCreated(){
        return response()->json(Order::with(['user','vehicle'])->where('status','creada')->orderBy('created_at','desc')->get(),200);
    }

}
