<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Http\Requests\VehicleStorageRequest;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Vehicle::orderBy('created_at','desc')->get(),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleStorageRequest $request)
    {
        $vehicle = Vehicle::create(
            [
                'brand' => $request->brand,
                'model' => $request->model,
                'year' => $request->year,
                'milage' => $request->milage,
                'available' => true
            ]
        );
        return response()->json($vehicle,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        return response()->json($vehicle,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        $vehicle->update($request->all());
        return response()->json($vehicle,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();
        return response()->json(null,204);
    }

    /**
     * Retorna todos los automoviles disponibles para renta
     * 
     * @return \Illuminate\Http\Response
     */

     public function getAvailable(){
         $vehicles = Vehicle::where('available',true)->get();
         return response()->json($vehicles,200);
     }
}
