<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturnVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'return_date' => 'required||date|date_format:Y/m/d|after_or_equal:pickup_date',
        ];
    }

    public function attributes()
    {
        return [
            'return_date' => 'Fecha de entrega del vehiculo',
        ];
    }

    public function messages()
    {
        return [
            'return_date.date' => 'La :attribute no cumple con el formato de fecha.',
            'return_date.date_format' => 'La :attribute no cumple con el formato de fecha Y/m/d.',
            'return_date.required' => 'La :attribute es obligatorio.',
        ];
    }
}
