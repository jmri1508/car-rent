<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required',
            'email' => 'required|email',
        ];
    }

    public function attributes()
    {
        return [
            'password' => 'contraseña del usuario',
            'email' => 'correo electronico del usuario',
        ];
    }

    /** 
     * Retorna los mensajes de las validaciones del request.
     *
     * @return array
     *
     */
    public function messages()
    {
        return [
            'password.required' => 'El :attribute es obligatorio.',
            'email.required' => 'El :attribute es obligatorio.',
            'email.email' => 'El :attribute debe ser un formato de correo electronico valido.',
        ];
    }
}
