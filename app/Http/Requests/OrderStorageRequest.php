<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderStorageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reservation_date' => 'required||date|date_format:Y/m/d|after_or_equal:today',
            'theoretical_return_date' => 'required||date|date_format:Y/m/d|after_or_equal:pickup_date',
            'pickup_date' => 'nullable',
            'return_date' => 'nullable',
            'vehicle_id' => 'required',
            'user_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'reservation_date' => 'Fecha teorica de la reservacion del vehiculo',
            'pickup_date' => 'Fecha de reservacion del vehiculo',
            'theoretical_return_date' => 'Fecha teorica para la entrega del vehiculo',
            'return_date' => 'Fecha de entrega del vehiculo',
            'status' => 'estatus de la orden',
            'user_id' => 'id del usuario',
            'vehicle_id' => 'id del vehiculo',
        ];
    }

    public function messages()
    {
        return [
            'reservation_date.date' => 'La :attribute no cumple con el formato de fecha.',
            'reservation_date.date_format' => 'La :attribute no cumple con el formato de fecha Y/m/d.',
            'reservation_date.required' => 'La :attribute es obligatorio.',
            'reservation_date.after_or_equal' => 'La :attribute tiene que ser mayor a la fecha de hoy.',
            'theoretical_return_date.date' => 'La :attribute no cumple con el formato de fecha.',
            'theoretical_return_date.date_format' => 'La :attribute no cumple con el formato de fecha Y/m/d.',
            'theoretical_return_date.required' => 'La :attribute es obligatorio.',
            'theoretical_return_date.after_or_equal' => 'La :attribute tiene que ser mayor o igual a la fecha de la reserva.',
            'user_id.required' => 'El :attribute es obligatorio.',
            'vehicle_id.required' => 'El :attribute es obligatorio.',
            'status.required' => 'El :attribute es obligatorio.',
        ];
    }
}
