<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStorageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'password' => 'required|confirmed|min:8|max:32',
            'email' => 'required|unique:users|email',
            'last_name' => 'required',
            'birthday' => 'required||date|date_format:Y/m/d',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre del usuario',
            'password' => 'contraseña del usuario',
            'email' => 'correo electronico del usuario',
            'last_name' => 'apellido del usuario',
            'birthday' => 'fecha de cumpleños del usuario',
        ];
    }

    /** 
     * Retorna los mensajes de las validaciones del request.
     *
     * @return array
     *
     */
    public function messages()
    {
        return [

            'name.required' => 'El :attribute es obligatorio.',
            'last_name.required' => 'El :attribute es obligatorio.',
            'password.required' => 'El :attribute es obligatorio.',
            'password.confirmed' => 'El :attribute debe ser enviada con el password confirmation.',
            'password.min' => 'El :attribute debe contener al menos 8 caracteres.',
            'password.max' => 'El :attribute debe contener maximo 32 caracteres.',
            'email.required' => 'El :attribute es obligatorio.',
            'email.unique' => 'El :attribute ya se encuentra registrado en la base de datos.',
            'birthday.date' => 'La :attribute no cumple con el formato de fecha.',
            'birthday.date_format' => 'La :attribute no cumple con el formato de fecha Y/m/d.',
            'birthday.required' => 'La :attribute es obligatorio.',
        ];
    }
}
