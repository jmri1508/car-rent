<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleStorageRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required',
            'model' => 'required',
            'year' => 'required|integer',
            'milage' => 'required|integer',
        ];
    }

    public function attributes()
    {
        return [
            'brand' => 'Marca del vehiculo',
            'model' => 'Modelo del vehiculo',
            'year' => 'Año del vehiculo',
            'milage' => 'Kilometraje del vehiculo',
        ];
    }

    public function messages()
    {
        return [
            'brand.required' => 'El :attribute es obligatorio.',
            'model.required' => 'El :attribute es obligatorio.',
            'year.required' => 'El :attribute es obligatorio.',
            'year.integer' => 'El :attribute debe ser un integer.',
            'milage.required' => 'El :attribute es obligatorio.',
            'milage.integer' => 'El :attribute debe ser un integer.',
        ];
    }
}
