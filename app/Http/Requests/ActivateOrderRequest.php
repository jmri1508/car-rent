<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pickup_date' => 'required||date|date_format:Y/m/d|after_or_equal:today',
        ];
    }

    public function attributes()
    {
        return [
            'pickup_date' => 'Fecha de reservacion del vehiculo',
        ];
    }

    public function messages()
    {
        return [
            'pickup_date.date' => 'La :attribute no cumple con el formato de fecha.',
            'pickup_date.date_format' => 'La :attribute no cumple con el formato de fecha Y/m/d.',
            'pickup_date.required' => 'La :attribute es obligatorio.',
            'pickup_date.after_or_equal' => 'La :attribute tiene que ser mayor a la fecha de hoy.',
        ];
    }
}
